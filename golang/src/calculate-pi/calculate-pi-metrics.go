package main

import (
	"fmt"
	log "github.com/cihub/seelog"
	"math"
	"runtime"
	"sort"
	"time"
)


func calculatePi(numTasks int, numElements int) (float64, time.Duration) {

	// out := make(chan float64, numTasks*numElements)
	out := make(chan float64)
	defer close(out)

	var acc float64 = 0.0

	start := time.Now()

	for taskNum:=0; taskNum<numTasks; taskNum++ {

		go func(taskNum_ int, numElements_ int) {
			var segmentValue float64 = 0.0;
			for i := taskNum_ * numElements_; i <= ((taskNum_ + 1) * numElements_ - 1); i++ {
				segmentValue += 4.0 * float64((1 - (i % 2) * 2)) / float64(2 * i + 1)
			}

			out <- segmentValue
		}(taskNum, numElements)
	}

	for taskNum:=0; taskNum<numTasks; taskNum++ {

		select {

		case segmentValue := <-out:
			acc += segmentValue

		case <-time.After(5000*time.Millisecond):
			log.Error("Timed out after 5 seconds.")
			break
		}
	}

	end := time.Now()

	et := end.Sub(start)

	return acc, et
}

var numProcs int;
var seelogConfigFile string = "seelog.xml"

func init() {

	logger, err := log.LoggerFromConfigAsFile(seelogConfigFile)

	if err != nil  {
		log.Error("Error reading the file " + seelogConfigFile)
	} else {
		log.ReplaceLogger(logger)
	}

	numProcs = runtime.NumCPU()

	log.Info(fmt.Sprintf("%d",numProcs) + " CPUs.")
	log.Info("Go version: " + runtime.Version())
	log.Info("GOARCH: " + runtime.GOARCH)
	log.Info("GOOS: " + runtime.GOOS)
}

func logStatistics(ets []int) {

		sort.IntSlice(ets).Sort()
		log.Info(ets)
		n := len(ets)

		average := 0.0
		for i := range ets {

			average += float64(ets[i])
		}
		average /= float64(n)

		deltaSquared := 0.0
		deltaCubed := 0.0
		for i := range ets {

			delta := float64(ets[i]) - average
			deltaSquared += delta*delta
			deltaCubed += deltaSquared*delta
		}
		variance := deltaSquared / float64(n-1)

		m3 := deltaCubed / float64(n)
		m2 := deltaSquared / float64(n)

		//	http://www.tc3.edu/instruct/sbrown/stat/shape.htm
		g1 := m3 / math.Pow(m2, 1.5)
		G1 := g1*math.Sqrt(float64(n*(n-1))) / float64(n-2)

		//	Same as G1
		// skewness := (float64(n) / float64((n-1)*(n-2)) ) * deltaCubed /  math.Pow(variance, 1.5)

		log.Info(fmt.Sprintf("Min: %d", ets[0]))
		log.Info(fmt.Sprintf("Max: %d", ets[n-1]))
		log.Info(fmt.Sprintf("Median: %d", ets[n/2]))
		log.Info(fmt.Sprintf("Average: %g", average))
		log.Info(fmt.Sprintf("StdDev: %g", math.Sqrt(variance)))
		// log.Info(fmt.Sprintf("g1: %g", g1))
		log.Info(fmt.Sprintf("Skewness (G1): %g", G1))
		// log.Info(fmt.Sprintf("Skewness: %g", skewness))

}


func main() {

	defer log.Flush()

	//	Warm up - ignore values
	runtime.GOMAXPROCS(numProcs)
	for j := 0; j<2; j++ {
		_, _ = calculatePi(10000, 10000)
	}

	numToAvg := 100
	for i := 1; i <= numProcs; i++ {

		runtime.GOMAXPROCS(i)

		ets := make([]int, numToAvg, numToAvg)
		for j := 0; j<numToAvg; j++ {
			_, et := calculatePi(10000, 10000)
			ets[j] = int(et/time.Millisecond)
		}

		log.Info("")
		// log.Inof(i + " CPUs")
		log.Info(fmt.Sprintf("%d CPUs", i))
		log.Info(ets)
		log.Flush()

		logStatistics(ets)
	}

}

