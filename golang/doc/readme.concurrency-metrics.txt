

You must have seelog installed to use this package:

    go get -u github.com/cihub/seelog

    https://github.com/cihub/seelog


To run:

	export GOPATH="$GOPATH;C:\path\to\concurrency-metrics\golang" - cygwin
or
	set GOPATH=C:\path\to\concurrency-benchmarks - DOS


	cd /c/path/to/concurrency-metrics/golang/src/calculate-pi - cygwin
	
