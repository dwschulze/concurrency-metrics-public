package concurrency.utils;

import org.apache.log4j.Logger;

public class EnvUtil {

	static Logger logger = Logger.getLogger(EnvUtil.class);

	public static void logEnv() {
		
		logger.info("Java version:  " + System.getProperty("java.version"));
		logger.info("Java VM:  " + System.getProperty("java.vm.name"));
		logger.info("OS:  " + System.getProperty("os.name") + ", " + System.getProperty("os.arch"));
		logger.info("Processors:  " + Runtime.getRuntime().availableProcessors());
	}

}
