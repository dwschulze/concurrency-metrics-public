package concurrency.utils;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;


public class StatisticsOnCollection<T extends Number & Comparable<T>> {

	static Logger logger = Logger.getLogger(StatisticsOnCollection.class);

	double average = 0.0;
	double variance = 0.0;
	T median;
	T min;
	T max;
	int num;
	String s = "";
	
	public StatisticsOnCollection(List<T> c) {
		
		Collections.sort(c);
		
		if ((c == null) || (c.size() < 1)) {
			
			s = "Null or empty List.";
		} else {
		
			num = c.size();
			double average_ = 0.0;
			for (T et : c) {
				average_ += et.doubleValue();
				s += et + ", ";
			}
			average_ /= c.size();
			average = average_;

			double variance_ = 0.0;
			
			for (T et : c) {
				double d = et.doubleValue() - average_;
				variance_ += d*d;
			}
			
			variance = variance_ / c.size();

			if (s.length() > 2)
				s = s.substring(0, s.length() - 2);
			
			min = c.get(0);
			max = c.get(c.size()-1);
			median = c.get(c.size()/2);
		}
	}

	public double getAverage() {
		return average;
	}

	public T getMedian() {
		return median;
	}

	public T getMin() {
		return min;
	}

	public T getMax() {
		return max;
	}

	public double getVariance() {
		return variance;
	}
	
	public void logStats() {
		
		logger.info(num + " values.");
		logger.info("min: " + min);
		logger.info("average: " + average);
		logger.info("median: " + median);
		logger.info("max: " + max);
		logger.info("StdDev: " + Math.sqrt(variance));
		logger.info(s);
	}

}
