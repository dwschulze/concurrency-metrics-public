package driver;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import message.Calculate;

import org.apache.log4j.Logger;

import scala.concurrent.duration.Duration;
import actor.Listener;
import actor.Master;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorFactory;
import concurrency.utils.EnvUtil;
import concurrency.utils.StatisticsOnCollection;


public class Driver {

	static Logger logger = Logger.getLogger(Driver.class);
	static final int numProcs = Runtime.getRuntime().availableProcessors();
	
	Duration duration;
	Map<Integer, List<Long>> numWorkersToDurationMap = new TreeMap<Integer, List<Long>>();
	
	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public Map<Integer, List<Long>> getNumWorkersToDurationMap() {
		return numWorkersToDurationMap;
	}

	public void calculate(
		final int nWorkers, 
		final int nElements,
		final int nMessages) {
		
//		logger.info("");
		logger.info("");
		logger.info(nWorkers + " workers, " + nElements + " elements, " + nMessages + " messages.");

		ActorSystem system = ActorSystem.create("Calculate-Pi-System");

		final ActorRef listener = system.actorOf(new Props(Listener.class), "listener");

		final Driver driver = this;
		ActorRef master = system.actorOf(new Props(new UntypedActorFactory() {
			public UntypedActor create() {
				return new Master(nWorkers, nMessages, nElements, listener, driver);
			}
		}), "master");

		master.tell(new Calculate());
			
		while (!master.isTerminated()) {
			
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				logger.error(e);
			}

		}
		
		if (duration != null) {
			logger.info("Duration: " + duration);
			
			if (numWorkersToDurationMap.get(nWorkers)==null)
				numWorkersToDurationMap.put(nWorkers, new ArrayList<Long>());
			
			numWorkersToDurationMap.get(nWorkers).add(duration.toMillis());
			
		} else
			logger.info("Duration is null.");
	}


	private void logStats() {
		
		for (Integer I : numWorkersToDurationMap.keySet()) {

			StatisticsOnCollection<Long> s = new StatisticsOnCollection<Long>(numWorkersToDurationMap.get(I));
			
			logger.info("");
			logger.info("");
			logger.info("=======================================================");
			logger.info("Number of workers " + I);
			s.logStats();
		}
		
	}

	public static void main(String[] args) {

		Driver d = new Driver();
		EnvUtil.logEnv();

		//	Warm up - discard results
		for (int j=0; j<2; j++) {
			
			for (int i=1; i<=numProcs; i++) {

				d.calculate(i, 10000, 10000);
			}
		}
		
		//	Discard the warm up values
		d.getNumWorkersToDurationMap().clear();

		for (int j=0; j<100; j++) {
			
			for (int i=1; i<=numProcs; i++) {

				d.calculate(i, 10000, 10000);
			}
		}
		
		d.logStats();
	}

}
