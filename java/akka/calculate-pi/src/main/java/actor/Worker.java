package actor;

import org.apache.log4j.Logger;
import message.Result;
import message.Work;
import akka.actor.UntypedActor;

public class Worker extends UntypedActor {

	static Logger logger = Logger.getLogger(Worker.class);

	private double calculatePiFor(int start, int nrOfElements) {

		double acc = 0.0;
		for (int i = start * nrOfElements; i <= ((start + 1) * nrOfElements - 1); i++) {
			acc += 4.0 * (1 - (i % 2) * 2) / (2 * i + 1);
		}
		return acc;
	}

	@Override
	public void onReceive(Object message) throws Exception {

		if (message instanceof Work) {
			
			Work work = (Work) message;
			double result = calculatePiFor(work.getStart(), work.getNumElements());
			getSender().tell(new Result(result), getSelf());
		} else {

			logger.info("Unknown message: " + message.getClass().getName());
			unhandled(message);
		}
	}

}
