package actor;


import message.Finished;
import org.apache.log4j.Logger;
import akka.actor.UntypedActor;


public class Listener extends UntypedActor {

	static Logger logger = Logger.getLogger(Listener.class);

	public void onReceive(Object message) {
		if (message instanceof Finished) {
			getContext().system().shutdown();
		} else {
			unhandled(message);
		}
	}

}
