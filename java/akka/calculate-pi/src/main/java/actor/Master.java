package actor;

import java.util.concurrent.TimeUnit;

import message.Calculate;
import message.Finished;
import message.Result;
import message.Work;

import org.apache.log4j.Logger;

import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinRouter;
import driver.Driver;

public class Master extends UntypedActor {

	static Logger logger = Logger.getLogger(Master.class);

	private final int numMessages;
	private final int numElements;
	private int numResults;

	private double pi;
	private long startTime;

	private final ActorRef listener;
	Driver driver;
	private final ActorRef workerRouter;

	public Master(
		final int numWorkers, 
		int numMessages, 
		int numElements,
		ActorRef listener, 
		Driver driver) {
		
		this.numMessages = numMessages;
		this.numElements = numElements;
		this.listener = listener;
		this.driver = driver;

		RoundRobinRouter router = new RoundRobinRouter(numWorkers);
		Props props = new Props(Worker.class);
		workerRouter = this.getContext().actorOf(props.withRouter(router), "workerRouter");
	}

	public void onReceive(Object message) {
		
		if (message instanceof Calculate) {
			
			if (startTime == 0l)
				startTime = System.currentTimeMillis();

			for (int start = 0; start < numMessages; start++)
				workerRouter.tell(new Work(start, numElements), getSelf());
			
		} else if (message instanceof Result) {

			Result result = (Result) message;
			pi += result.getValue();
			numResults++;
			if (numResults == numMessages) {
				
				long ms = System.currentTimeMillis() - startTime;
				Duration duration = Duration.create(ms, TimeUnit.MILLISECONDS);
				
				listener.tell(new Finished(), getSelf());
				driver.setDuration(duration);
				// Stops this actor and all its supervised children
				getContext().stop(getSelf());
			}
		} else {

			logger.info("Unknown message: " + message.getClass().getName());
			unhandled(message);
		}
	}

}
