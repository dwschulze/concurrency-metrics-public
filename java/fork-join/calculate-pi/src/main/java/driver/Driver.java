package driver;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import task.PiTask;
import concurrency.utils.EnvUtil;
import concurrency.utils.StatisticsOnCollection;


public class Driver {

	static Logger logger = Logger.getLogger(Driver.class);
	
	static int numTasks = 10000;
	static int numElements = 10000;

	public static void main(String[] args) {
		
		EnvUtil.logEnv();

		ArrayList<Long> ets = new ArrayList<Long>();
		
		for (int i=0; i<102; i++) {
			
			ForkJoinPool pool=new ForkJoinPool();
			PiTask t = new PiTask(numTasks, -1, numElements);

			pool.execute(t);
			
			do {
				logger.info("");
				logger.info("------------------------------------");
				logger.info("Parallelism: " + pool.getParallelism());
				logger.info("Active Threads: " + pool.getActiveThreadCount());
				logger.info("Queued Task Count: " + pool.getQueuedTaskCount());
				logger.info("Steal Count: " + pool.getStealCount());
	
				try {
					TimeUnit.MILLISECONDS.sleep(100);
				} catch (InterruptedException e) {
					logger.error(e);
				}
				
			} while (!t.isDone());
	
			pool.shutdown();
			
			try {
				pool.awaitTermination(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				logger.error(e);
			}
			
			//	Skip the first two
			if (i > 1)
				ets.add(t.getET());
			else
				logger.info("Discarding result " + i);

			try {
				logger.info("ET = " + t.getET() + " ms.  Pi = " + t.get());
			} catch (InterruptedException | ExecutionException e) {
				logger.error(e);
			}
	
		}	//	for (int i=0; i<12; i++)
		
		logger.info("");
		logger.info("");
		logger.info("=======================================================");
		StatisticsOnCollection<Long> s = new StatisticsOnCollection<Long>(ets);
		s.logStats();
	}
}
