package task;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RecursiveTask;
import org.apache.log4j.Logger;


public class PiTask extends RecursiveTask<Double> {

	static Logger logger = Logger.getLogger(PiTask.class);

	private static final long serialVersionUID = 1L;
	int numTasks;
	int startTaskNum;
	int numElements;
	Date start = null;
	Date end = null;

	
	public PiTask(int numTasks, int startTaskNum, int numElements) {
		
		this.numTasks = numTasks;
		this.startTaskNum = startTaskNum;
		this.numElements = numElements;
		if (numTasks > 1)
			start = new Date();
	}
	
	@Override
	protected Double compute() {
		
		double d = 0.0;
		if (numTasks > 1) {
			
			//	Recurse
			
			ArrayList<PiTask> l = new ArrayList<PiTask>();
			
			for (int i=0; i<numTasks; i++) {
				
				PiTask pt = new PiTask(1, i, numElements);
				l.add(pt);
			}

			invokeAll(l);
			
			try {
				d = sumComputeResults(l);
				end = new Date();
			} catch (InterruptedException | ExecutionException e) {
				logger.error(e);
			}

		} else {
			
			//	Compute
			
			d = calculatePiSegment();
			
		}
		
		return d;
	}
	
	public long getET() {
		
		long l = -1l;
		
		if ((start !=null) && (end != null)) {
			
			l = end.getTime() - start.getTime();
		}
		
		return l;
	}

	private double sumComputeResults(ArrayList<PiTask> l) throws InterruptedException, ExecutionException {

		double d = 0.0;
		
		for (PiTask t : l)
			d += t.get();
		
		return d;
	}

	private double calculatePiSegment() {
		double acc = 0.0;
		for (int i = startTaskNum * numElements; i <= ((startTaskNum + 1) * numElements - 1); i++) {
			acc += 4.0 * (1 - (i % 2) * 2) / (2 * i + 1);
		}
		return acc;
	}

}
